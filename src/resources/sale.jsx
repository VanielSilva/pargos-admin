import * as React from 'react';
import { List, Filter, Datagrid, Edit, Create, TabbedForm, FormTab, ReferenceField, FunctionField, TextField, NumberField, ChipField, BooleanField, EditButton, ArrayInput, SimpleFormIterator, BooleanInput, TextInput, DateInput, required, AutocompleteInput, ReferenceInput, DateField } from 'react-admin';
import Icon from '@material-ui/icons/Receipt';

import Toolbar from '../components/toolbar';

const ViewTitle = ({ record }) => (
	<span>Venda {record ? `#${record.id}` : ''}</span>
);

const ViewFilter = (props) => (
	<Filter {...props}>
		<ReferenceInput label='Vendedor' source='userId' reference='user' alwaysOn>
			<AutocompleteInput optionText='person.name' />
		</ReferenceInput>
	</Filter>
);

const resource = {
	name: 'sale',
	options: {
		label: 'Vendas',
	},
	icon: Icon,
	list: (props) => (
		<List {...props} bulkActionButtons={false} filters={<ViewFilter />}>
			<Datagrid>
				<TextField label='#' source='id' sortable={false} />
				<ReferenceField label='Vendedor' source='userId' reference='user' sortable={false}>
					<FunctionField render={u => `#${u.id}${u.person.name ? ` - ${u.person.name}` : ''}`} />
				</ReferenceField>
				<ChipField label='Plano' source='plan.name' sortable={false} />
				<NumberField label='Valor' source='plan.value' sortable={false} options={{ style: 'currency', currency: 'BRL' }} />
				<BooleanField label='Confirmada' source='isConfirmed' sortable={false} />
				<DateField label='Data' source='createdAt' />
				<EditButton basePath='/sale' />
			</Datagrid>
		</List>
	),
	edit: (props) => (
		<Edit {...props} title={<ViewTitle />} transform={data => ({
			isConfirmed: data.isConfirmed,
			email: data.email,
			person: data.person,
			address2: data.address2,
			phone2: data.phone2,
			dependents: data.dependents,
		})}>
			<TabbedForm toolbar={<Toolbar />}>
				<FormTab label='Venda'>
					<TextField label='#' source='id' />
					<TextField label='Plano' source='plan.name' />
					<DateField label='Data' source='createdAt' />
					<TextInput label='E-mail' source='email' />
					{props.permissions === 'root' && <BooleanInput label='Confirmada' source='isConfirmed' />}
				</FormTab>

				<FormTab label='Pessoal'>
					<TextInput label='Nome' source='person.name' />
					<TextInput label='Sexo' source='person.sex' />
					<TextInput label='RG' source='person.rg' />
					{/* <TextInput label='RG info' source='person.rgInfo' /> */}
					<TextInput label='CPF' source='person.cpf' />
					<TextInput label='Nacionalidade' source='person.nationality' />
					<DateInput label='Data de nascimento' source='person.birthday' />
					<TextInput label='Estado civil' source='person.maritalStatus' />
					<TextInput label='Profissão' source='person.profission' />
					<TextInput label='Telefone' source='person.phone' />
				</FormTab>

				<FormTab label='Endereço'>
					<TextInput label='CEP' source='person.address.cep' />
					<TextInput label='Estado' source='person.address.uf' />
					<TextInput label='Cidade' source='person.address.city' />
					<TextInput label='Bairro' source='person.address.district' />
					<TextInput label='Rua' source='person.address.street' />
				</FormTab>

				<FormTab label='Endereço Adicional'>
					<TextInput label='CEP' source='address2.cep' />
					<TextInput label='Estado' source='address2.uf' />
					<TextInput label='Cidade' source='address2.city' />
					<TextInput label='Bairro' source='address2.district' />
					<TextInput label='Rua' source='address2.street' />
					<TextInput label='Telefone' source='phone2' />
				</FormTab>

				<FormTab label='Dependentes'>
					<ArrayInput label='Dependentes' source='dependents'>
						<SimpleFormIterator>
							<TextInput label='Nome' />
						</SimpleFormIterator>
					</ArrayInput>
				</FormTab>
			</TabbedForm>
		</Edit>
	),
	create: (props) => (
		<Create title='Criar venda' {...props}>
			<TabbedForm>
				<FormTab label='Venda'>
					<ReferenceInput label='Plano' source='planId' reference='plan' validate={[required()]}>
						<AutocompleteInput optionText='name' />
					</ReferenceInput>
					<TextInput label='E-mail' source='email' />
				</FormTab>

				<FormTab label='Pessoal'>
					<TextInput label='Nome' source='person.name' />
					<TextInput label='Sexo' source='person.sex' />
					<TextInput label='RG' source='person.rg' />
					{/* <TextInput label='RG info' source='person.rgInfo' /> */}
					<TextInput label='CPF' source='person.cpf' />
					<TextInput label='Nacionalidade' source='person.nationality' />
					<DateInput label='Data de nascimento' source='person.birthday' />
					<TextInput label='Estado civil' source='person.maritalStatus' />
					<TextInput label='Profissão' source='person.profission' />
					<TextInput label='Telefone' source='person.phone' />
				</FormTab>

				<FormTab label='Endereço'>
					<TextInput label='CEP' source='person.address.cep' />
					<TextInput label='Estado' source='person.address.uf' />
					<TextInput label='Cidade' source='person.address.city' />
					<TextInput label='Bairro' source='person.address.district' />
					<TextInput label='Rua' source='person.address.street' />
				</FormTab>

				<FormTab label='Endereço Adicional'>
					<TextInput label='CEP' source='address2.cep' />
					<TextInput label='Estado' source='address2.uf' />
					<TextInput label='Cidade' source='address2.city' />
					<TextInput label='Bairro' source='address2.district' />
					<TextInput label='Rua' source='address2.street' />
					<TextInput label='Telefone' source='phone2' />
				</FormTab>

				<FormTab label='Dependentes'>
					<ArrayInput label='Dependentes' source='dependents'>
						<SimpleFormIterator>
							<TextInput label='Nome' />
						</SimpleFormIterator>
					</ArrayInput>
				</FormTab>
			</TabbedForm>
		</Create>
	),
};

export default resource;
