import {
	httpClient
} from './data';
import {
	Queries
} from '../queries';

const authProvider = {
	async login({
		username: email,
		password
	}) {
		const {
			json: {
				data,
				errors,
			}
		} = await httpClient(Queries.LOGIN, {
			email,
			password,
		});
		if (errors && errors.length > 0) {
			throw new Error(errors[0].message);
		}
		localStorage.setItem('auth', data.result.token);
		localStorage.setItem('user', JSON.stringify(data.result.user));
		localStorage.setItem('role', data.result.user.role);
	},

	async logout() {
		localStorage.removeItem('auth');
		localStorage.removeItem('user');
		localStorage.removeItem('role');
	},

	async checkError({
		status
	}) {
		if (status === 401 || status === 403) {
			localStorage.removeItem('auth');
			localStorage.removeItem('user');
			localStorage.removeItem('role');
			return Promise.reject();
		}
		return Promise.resolve();
	},

	async checkAuth() {
		return localStorage.getItem('auth') ?
			Promise.resolve() :
			Promise.reject();
	},

	async getIdentity() {
		try {
			const {
				id,
				person: {
					name: fullName
				},
				email,
			} = JSON.parse(localStorage.getItem('user'));
			return Promise.resolve({
				id,
				fullName,
				email,
			});
		} catch (error) {
			return Promise.reject(error);
		}
	},

	async getPermissions() {
		const role = localStorage.getItem('role');
		return role ? Promise.resolve(role) : Promise.reject();
	},
};

export default authProvider;