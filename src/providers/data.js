import {
	fetchUtils
} from 'react-admin';
import {
	list,
	get,
	create,
	update,
} from '../queries';

// const apiUrl = (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') ? 'http://localhost:8080/' : 'https://pargos.herokuapp.com/';
const apiUrl = 'https://pargos.herokuapp.com/';
export const httpClient = (query, variables = {}) => {
	const headers = new Headers({
		Accept: 'application/json'
	});

	const token = localStorage.getItem('auth');
	if (token) {
		headers.set('Authorization', `Bearer ${token}`);
	}

	return fetchUtils.fetchJson(apiUrl, {
		method: 'POST',
		headers: headers,
		body: JSON.stringify({
			query,
			variables,
		}),
	});
};

const checkError = (errors) => {
	if (errors && errors.length > 0) {
		throw new Error(errors[0].message);
	}
};

const dataProvider = {
	async getList(resource, params) {
		const {
			json: {
				data,
				errors,
			},
		} = await httpClient(list(resource), {
			...params.filter,
			pagination: {
				page: params.pagination.page - 1,
				limit: params.pagination.perPage,
			},
		});
		checkError(errors);
		return ({
			data: data.result.nodes,
			total: data.result.pageInfo.count,
		});
	},

	async getOne(resource, params) {
		const {
			json: {
				data,
				errors,
			},
		} = await httpClient(get(resource), params);
		checkError(errors);
		return ({
			data: data.result,
		});
	},

	async getMany(resource, params) {
		const data = await Promise.all(
			params.ids.map((id) => this.getOne(resource, {
				id
			}).then(result => result.data)),
		);
		return {
			data
		};
	},

	async create(resource, params) {
		const {
			json: {
				data,
				errors,
			},
		} = await httpClient(create(resource), params);
		checkError(errors);
		return ({
			data: data.result,
		});
	},

	async update(resource, params) {
		const {
			json: {
				data,
				errors,
			},
		} = await httpClient(update(resource), {
			id: params.id,
			data: {
				...params.data,
				id: undefined,
			},
		});
		checkError(errors);
		return ({
			data: data.result,
		});
	},
};

export default dataProvider;